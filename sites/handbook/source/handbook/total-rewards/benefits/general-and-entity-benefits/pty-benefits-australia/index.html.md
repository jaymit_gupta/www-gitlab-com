---
layout: handbook-page-toc
title: "PTY LTD Benefits"
description: "GitLab PTY Australia benefits specific to Australia based team members."
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-group/).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## GitLab PTY Australia Specific Benefits

### Medical
GitLab does not plan to offer Private Health Insurance at this time because Australians can access the public health system for free or at a lower cost through Medicare (funded by tax).

Please note, Medicare doesn't cover all costs and [other health services](https://www.servicesaustralia.gov.au/individuals/subjects/whats-covered-medicare/health-care-and-medicare). Depending on the treatment, tests required and medications, individuals may still have to pay out of pocket. Some individuals may also have to pay a [Medicare levy and a Medicare levy surcharge](https://www.ato.gov.au/Individuals/Medicare-levy/). These individuals can avoid the surcharge by taking out private health insurance.

### Superannuation
GitLab will make superannuation contributions directly to the team member's nominated super fund according to the Australian Government Super Guarantee rate which is currently set at 9.5% of the team member's total salary. Super is on top of the salary listed in the compensation calculator/contract.

#### Superannuation payments due dates

Super payment due dates has been a common topic in [#loc_australia](https://app.slack.com/client/T02592416/CHHFS9DR7). However, there is no reason to worry when payments do not happen monthly as they may be done quarterly.

The Australian Taxation Office (ATO) states the following:

> You pay super for eligible employees calculated from the day they start with you. You must make the payments at least four times a year, by the quarterly due dates.

Payments due dates can be found on the [ATO website](https://www.ato.gov.au/business/super-for-employers/paying-super-contributions/when-to-pay-super/).

#### Superannuation Salary Sacrifice
Team members in Australia have the option to make concessional super contributions by sacrificing some of their pre-tax pay and arrange to have it paid into their superfund instead. The combined total of GitLab and salary sacrificed contributions [must not be more than $25,000](https://moneysmart.gov.au/grow-your-super/super-contributions) per financial year.

*Salary Sacrifice FAQ*

* How do I make concessional contributions to my superannuation?
  * Email total-rewards@gitlab.com with the percentage or amount of your salary that you would like to sacrifice.
* Can I change the amount or opt out?
  * Yes, if you wish to change the amount/percentage or opt out, simply send total-rewards@gitlab.com an email.
* Is it possible to start from any month?
  * Yes, it would be processed on the next available payroll.


### Life insurance
GitLab does not plan to offer life insurance at this time as Australians can access [government payments and services](https://www.humanservices.gov.au/individuals/subjects/payments-people-living-illness-injury-or-disability) if they get ill, injured or have a disability. Most Australians who choose to have life insurance take out cover from their super fund.


### On-Call (Engineering only)

Certain groups in the Engineering division are expected to participate in on-call for business continuity and/or customer support. For more details on the on-call procedure, please reference the [on-call page](https://about.gitlab.com/handbook/on-call/). The purpose of this section is to provide an overview of **time in lieu that will be offered for the Development and Infrastructure departments effective February 1, 2021.** We will continue to review and implement to other departments within Engineering iteratively.

With our global team, we strive for a "follow the sun" model for on-call rotation. This way, team members can participate in on-call during their daytime rather than their nighttime in their respective regions (APAC, EMEA and AMER). In APAC, the majority of the team members are located in Australia. We want to ensure engineers in Australia are 1) eligible for on-call shifts, and 2) stay aligned with local laws to ensure proper rest/recovery time after on-call shifts.

Engineers based in Australia who are part of the Development and Infrastructure departments will be eligible for time in lieu after on-call shifts. Time in lieu is time off from work that is granted to team members to work outside of standard working hours (I.E. ordinary hours).

##### How much time in lieu will I receive for volunteering for on-call shifts?

* 4-hour shift = ½ day time in lieu
* 8-hour shift = 1 day time in lieu
* 24-hour shift = 3 days time in lieu 

For weekend shifts please be particularly mindful about managing your time to **ensure you are not exceeding a 38 hour work-week**, and feel free to leverage our [unlimited PTO policy](https://about.gitlab.com/handbook/paid-time-off/#paid-time-off) to complement time in lieu. 

_Note: The full shift (state of readiness) is compensated by time in lieu - rather than only the "actual time worked"_

##### What should I do when I want to take more time off after an on-call shift?

If you need additional time to ensure a good work/life balance, on top of the time in lieu, you can leverage our Unlimited [PTO policy as described here](https://about.gitlab.com/handbook/paid-time-off/#paid-time-off). 

##### When can time in lieu be taken?

Time in lieu should be taken _within 2 weeks after completion of the on-call shift_. We want to ensure that time in lieu is taken close to the on-call shift to ensure that team members have sufficient time to rest.

##### Do all on-call shifts result in time in lieu?

No, only on-call shifts outside of working hours (I.E. weekends, evenings, etc.). _Please note that not all teams require weekend or evening on-call shifts, so this may not be applicable for all._

##### How do I request time in lieu?

Time in lieu should be requested via [PTO by Roots](/handbook/paid-time-off/#pto-by-roots) by selecting the `On-Call Time in Lieu` option.


### GitLab PTY Australia Annual, Sick and Carer's Leave, and Other Statutory Leaves

#### Annual Leave
* Full-time team members are entitled to 4 weeks paid annual leave in accordance with the Fair Work Act 2009 (Cth) per year of continuous service.  This amount will be pro-rated for part-time team members by reference to the team member’s ordinary hours of work. 
* Annual leave accrues progressively and accumulates from year to year.
* This leave runs concurrently with GitLab PTO.  Team members should select `Vacation` in PTO by Roots to ensure that leave entitlements are properly tracked.  For team members who are not covered by a modern award or enterprise agreement, they may be asked to take a period of paid annual leave if such request is reasonable, such as when a team member has accrued an excessive amount of paid annual leave.

#### Personal/Carer’s Leave
* Team members will be entitled to personal/carer’s leave in accordance with the Fair Work Act 2009 (Cth) as varied from time to time.  Currently that entitlement is 10 days of paid personal/carer’s leave for each year of continuous service for full-time team members and pro rata for part-time team members based on ordinary hours of work.
* Personal/carer’s leave is cumulative and accrues progressively.
* This leave runs concurrently with GitLab PTO.  Team members should select `Out Sick` in PTO by Roots to ensure that leave entitlements are properly tracked.  If you wish to take Carer's Leave but don't want to change your Slack status to Out Sick, send your Carer's Leave request to total-rewards@gitlab.com and the Total Rewards team will manually add your leave to BambooHR and notify payroll.

#### Long Service Leave
* Team members may be eligible for long service leave in accordance with state/territory based legislation as varied from time to time.
* This leave runs concurrently with GitLab PTO.  Team members should select `Vacation` in PTO by Roots to ensure that leave entitlements are properly tracked.

#### Other Types of Leave
<details>
  <summary markdown="span">Family and Domestic Violence Leave</summary>

* All team members are entitled to 5 days of unpaid family and domestic violence leave each year. This leave will run concurrently with GitLab PTO. Team members should notify Total Rewards (total-rewards@gitlab.com) if they need to make use of this leave.
* For more information on Family and Domestic Violence Leave, please visit the Fair Work Ombudsman Website.
* Resources: 
  * [1800RESPECT](https://www.1800respect.org.au/)
  * [Safe Steps](https://www.safesteps.org.au/)
  * [Victims Services](https://www.victimsservices.justice.nsw.gov.au/)
  * [Domestic Violence Resource Centre Victoria](https://www.dvrcv.org.au/help-advice)
  * [JobWatch](https://jobwatch.org.au/)
  * [MensLine Australia](https://mensline.org.au/)
  * [Men’s Referral Service](https://ntv.org.au/)
</details>

<details>
  <summary markdown="span">Compassionate and Bereavement Leave</summary>

* All employees are entitled to 2 days of paid compassionate leave in the event that an immediate family or household member dies or suffers a life threatening illness or injury.
* This leave runs concurrently with GitLab PTO. Team members should select Bereavement Leave in PTO by Roots to ensure that leave entitlements are properly tracked.

</details>

<details>
  <summary markdown="span">[Public Holidays](https://www.fairwork.gov.au/leave/public-holidays)</summary>

* All team members are entitled to at least their base pay rate for all hours worked on public holidays.
* Team members who take time off for public holidays are entitled to their base pay rate for the ordinary hours they would have worked. GitLab encourages all team members to take time off for the holidays they observe.
* Team members must select `Public Holiday` in PTO by Roots to ensure that statutory entitlements are properly tracked.

</details>

<details>
  <summary markdown="span">Community Service Leave</summary>

* All team members are entitled to unpaid community service leave for voluntary emergency management activities. Team members are also entitled to paid community service leave for jury duty.
* A team member engages in a voluntary emergency management activity if:
  * The activity involves dealing with an emergency or natural disaster
* The team member engages in the activity on a voluntary basis if:
  * The team member was either requested to engage in an activity, or it would be reasonable to expect that such a request would have been made if circumstances had permitted, and
  * The team member is a member of, or has a member-like association with a recognized emergency management body. More information can be found on the [FairWork Ombudsman Website](https://www.fairwork.gov.au/leave/community-service-leave).

Jury Duty
* Team members are entitled to “make-up pay” for the first 10 days of jury selection and jury duty. GitLab will supplement the difference between the amount that the team member has been paid and their ordinary pay to ensure that the team member receives 100% of their salary for the first 10 days of jury duty/selection.
* This leave runs concurrently with GitLab PTO. Team members must select `Mandatory Civilian Service` in PTO by Roots to ensure that statutory entitlements are properly tracked.
* Team members must notify their manager of the period or expected period of leave as soon as possible and provide evidence showing they attended jury selection or jury duty.
</details>

### GitLab PTY Australia Parental Leave

**Statutory Leave Entitlement**

All team members are entitled to up to 12 months of unpaid parental leave if they have been at GitLab for at least 12 months. They can also request up to 12 months of additional unpaid parental leave.

**Australian Government Parental Leave Payment**
* For team members with a yearly salary of $150,000 AUD or less, and who meet various other tests including a work test and residency rules.  The parent must also be the primary carer of the child:
  * All payments are managed by Services Australia (Federal government) and some employees will receive payment directly from Services Australia.
  * Only certain deductions can be made from Parental Leave Pay under the Australian Government Paid Parental Leave Scheme.
* For team members with a yearly salary above $150,000 AUD:
  * If you are eligible, you will receive 100% paid Parental Leave from GitLab for up to 16 weeks.

**Applying for Parental Leave in Australia**
* To initiate your parental leave, submit your time off by selecting the Parental Leave category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with GitLab's Parental Leave policy.
* If you are applying for government-funded Parental Leave Pay, you can apply up to 3 months before your child's due date.

**Record-keeping for paid Parental Leave:**
* In addition to the usual record-keeping requirements, employers that have team members getting government-funded Parental Leave Pay also have to keep the following records:
  *  the amount of Parental Leave Pay funding received from the government for each team member and the period it covers
  * the date each parental leave payment was made to the team member
  * the period each payment covers
  * the gross amount of the payment
  * the net amount paid and the amount of income tax withheld (including other payments, if any, were made)
  * a statement identifying the payment as Parental Leave Pay under the Australian Government Paid Parental Leave Scheme
  * the amount of any deductions made from each payment.
  
**Pay slips for Parental Leave payments:**
* Team members who get Australian Government Parental Leave Pay have to be given a pay slip for each payment. The pay slip must specify that the payments are Parental Leave Pay under the Australian Government Paid Parental Leave Scheme.
* Ordinary pay slip requirements apply to pay slips given to team members getting government-funded Parental Leave Pay. 



##  GitLab PTY New Zealand Specific Benefits

### Accounting Fee Reimbursement
New Zealand team members are eligible for a one time reimbursement of up to $750 NZD for accounting or tax-related fees. The purpose of this reimbursement is to help our team members reconcile any accounting activities and tax returns from when they were independent contractors through GitLab BV or contracted through CXC. Since tax-related activities will be less complex after the transition to our team members' employment under the PTY LTD entity, this reimbursement benefit will only be valid until December 2021. To get reimbursed for this expense, please follow the [Expense Reimbursement process](/handbook/spending-company-money/#expense-policy).

### Annual Leave
Team members are entitled to a minimum of four weeks’ annual leave per year. Annual leave does not expire and can be carried over each year, but GitLab may set expectations on how much annual leave employees may accumulate and may require employees to take entitled leave with 14 days notice. Annual leave runs concurrently with GitLab PTO. Team members must select the `Vacation` option in PTO by Roots when taking vacation time to ensure that annual leave entitlement is properly tracked.

### On-Call (Engineering-only)

New Zealand employment law requires that work on a formal [Public Holiday](https://www.govt.nz/browse/work/public-holidays-and-work/public-holidays-and-anniversary-dates/), including [on-call](https://www.employment.govt.nz/leave-and-holidays/public-holidays/employees-working-shifts-or-on-call/), is compensated with time-in-lieu. Overtime pay is also required in certain situations, for example: whether the on-call shift fell on a "normal" working day or not, and whether the team member was called to work during their on-call shift. The policy linked above covers for the [time-in-lieu requirement](/handbook/total-rewards/benefits/general-and-entity-benefits/pty-benefits-australia/#on-call-engineering-only) but we do not yet have a set mechanism for handling overtime pay requirements. 

New Zealand-based team members *may not be on-call on a New Zealand Public Holiday*, and must seek/find a replacement for any shifts where they may be indicated as being on call on a Public Holiday. To ensure we remain compliant with local laws we team members to consider local laws when volunteering for/accepting shifts. 

The process is as follows:

1. When scheduling we should avoid on-call taking/assigning on-call shifts on Public Holidays. A "Public Holiday" will be deemed as any day included in the official holidays on [this website](https://www.govt.nz/browse/work/public-holidays-and-work/public-holidays-and-anniversary-dates/). 
1. If a team member is assigned for an on-call shift that falls on a Public Holiday, they need to align with their manager and try to find a replacement for that shift. 
If it’s impossible to find a replacement and the team member ends up taking the on-call shift on a Public Holiday, the process to follow is:

* Team member should obtain manager approval for taking the shift.
* If the team member is not called in to work during their on-call shift, a regular time-in-lieu compensation for the shift should be applied.  
* If the team member is called in to work during their on call shift, they are responsible for recording their hours worked during the shift and report to payroll/manager in order for overtime payment processing. 

_Note: To stay in compliance with local laws we would follow [the New Zealand government website](https://www.employment.govt.nz/leave-and-holidays/public-holidays/employees-working-shifts-or-on-call/) with regards to compensation for worked time during on-call._


### Sick Leave

Team members in New Zealand are eligible to take paid time off according to our [PTO policy](https://about.gitlab.com/handbook/paid-time-off/). Team members in New Zealand are entitled to 5 days' sick leave after six months’ current continuous employment with GitLab. For each 12-month period after meeting the above criteria, each team member gets at least five days’ sick leave. If in any year the team member doesn’t meet the criteria, then they don’t get any new sick leave entitlement, but can use their sick leave balance which may have carried over. An employee may re-qualify for sick leave as soon as they meet the criteria.  The maximum amount of sick leave that can be accumulated under the Holidays Act 2003 is 20 days.

* Sick Leave runs concurrently with GitLab PTO. Team members must designate any time off for illness as `Out Sick` in PTO by Roots to ensure that sick leave entitlement is properly tracked.

* In accordance with the Holidays Act, GitLab may require a medical certificate if you have been absent due to illness. 

* Unused sick leave will not be paid out to the team member upon termination of employment.

**Injured Leave**
* When the team member is taking leave for the first week of a non-work accident, they can use sick leave and/or annual leave if they have any. This leave runs concurrently with GitLab PTO. Team members must designate time off for injury as `Out Sick` or `Vacation` in PTO by Roots if they wish to use sick leave or annual leave for the first week of an injury. Team members must notify Total rewards (total-rewards@gitlab.com) as soon as possible when they will need to go out on injured leave.

* If the leave will last longer than five days and is [covered by the Accident Compensation Corporation (ACC) scheme](https://www.acc.co.nz/im-injured/what-we-cover/), GitLab will top up the ACC payment from 80 to 100% for the first 25 days.

### GitLab PTY New Zealand Parental Leave 

#### Statutory Leave Entitlement
**Primary Carer Leave**
* Team members who have been employed for at least six months and become a “Primary Carer” as defined under the Parental Leave and Employment Protection Act 1987 can receive government-funded parental leave pay (Primary Carer Leave).
* A Primary Carer is either the biological mother OR a person (that is not the biological mother or her spouse/partner) who will take primary responsibility for the care, development, and upbringing of a child under six years old on a permanent basis.
* Primary Carer Leave can last up to 26 weeks and must be taken in one continuous period.
* Primary Carer Leave typically starts on the due date or date of birth, but may begin up to 6 weeks earlier if arrangements are made between the team member and manager.

**Parental Leave**

Extended Leave
* Team members who have been employed for at least six months, but less than twelve and are eligible under the Parental Leave and Employment Protection Act 1987 are entitled to up to a total of 26 weeks of unpaid leave (less time taken for Primary Carer Leave, if applicable).
* Team members who have been employed for at least twelve months and are eligible under the Parental Leave and Employment Protection Act 1987 are entitled to up to 52 weeks of unpaid leave (less time taken for Primary Carer Leave. A maximum of 26 weeks may be deducted from extended leave).

Special Leave
* Team members who are pregnant can also take up to 10 days of unpaid special leave for pregnancy-related reasons such as antenatal classes, scans, or midwife appointments. This is in addition to primary carer leave. 
* This leave will run concurrently with GitLab PTO. If you do not want to select `Out Sick` in PTO by Roots, you may email total-rewards@gitlab.com with the date(s) that you will be taking special leave. 

Partner Leave
* If you are a spouse or partner of a primary carer and have been employed for at least six months, but less than twelve months, you are entitled to one week of unpaid partner's leave. 
* If you are a spouse or partner of a primary carer and you have been employed for at least twelve months, you are entitled to two weeks of unpaid partner's leave. 
* Partner Leave will run concurrently with GitLab Parental Leave. If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), you will receive 100% paid Parental Leave from GitLab for up to 16 weeks.
* Team members must select `Parental Leave` in PTO by Roots to ensure that their time off is properly tracked.

#### Applying for Parental Leave in New Zealand
* To initiate your parental leave, submit your time off by selecting the `Parental Leave` category in PTO by Roots at least 30 days before your leave starts. Please familiarize yourself with [GitLab's Parental Leave policy](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).
* To apply for government-funded Parental Leave pay, please submit your application via [IRD](https://www.ird.govt.nz/paid-parental-leave/apply). Total Rewards will have to fill out the "Employer Declaration" part of the [form](https://www.ird.govt.nz/-/media/project/ir/home/documents/forms-and-guides/ir800---ir899/ir880/ir880-2020.pdf).
* If you are [eligible](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave), GitLab will supplement the government-funded payment to ensure that you receive 100% pay for up to 16 weeks of your Parental Leave.

#### Other Types of Leave

<details>
  <summary markdown="span">Stress Leave</summary>

* A team member with workplace stress that amounts to an illness may take sick leave. The [ordinary conditions for sick leave](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/pty-benefits-australia/#sick-leave) apply. If the team member has been sick for a period of 3 or more consecutive days, the team member may be asked to provide a medical certificate.
* This leave runs concurrently with GitLab PTO. Team members must designate Stress Leave as `Out Sick` in PTO by Roots to ensure that statutory entitlements are properly tracked.

</details>

<details>
  <summary markdown="span">Leave for Defence Force Volunteers</summary>

* Under the Volunteers Employment Protection Act 1973, all team members who are members of the Armed Forces are entitled to unpaid job-protected leave during military training and/or service. 
* This leave runs concurrently with GitLab PTO. Team members must email Total Rewards (total-rewards@gitlab.com) as soon as possible to notify them of the first date of leave and, if possible, the end date of their leave.

</details>

<details>
  <summary markdown="span">Election Voting Leave</summary>

* Team members are entitled to 2 hours of paid time off to vote in general (parliamentary) elections on polling day if they are registered as an elector of the district and have not had a reasonable opportunity to vote before starting work. GitLab encourages you to [take time off to vote](https://about.gitlab.com/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off) or volunteer at the polls. 
* This leave runs concurrently with GitLab PTO. Team members must select `Mandatory Civilian Service` in PTO by Roots to ensure that statutory entitlements are properly tracked.

</details>

<details>
  <summary markdown="span">Jury Service</summary>

* All team members who have been called for jury service are entitled to unpaid leave for the duration of the service. Team members who attend jury service receive an attendance fee from the Ministry of Justice. 
* Jury Service Leave runs concurrently with GitLab PTO. Team members must select `Mandatory Civilian Service` in PTO by Roots to ensure that statutory entitlements are properly tracked.

</details>

<details>
  <summary markdown="span">Bereavement Leave</summary>

* All team members who have been employed for six months are entitled to three days of paid leave in the event of the death of a spouse or partner, parent, child, sibling, grandparent, grandchild, or parent of a spouse or partner, and one day in the event of the death of another person not previously listed.
* This leave runs concurrently with GitLab PTO. Team members must designate this time off as `Bereavement Leave` in PTO by Roots to ensure that statutory entitlements are properly tracked.
* All team members who have been employed for six months are entitled to three days paid leave if a pregnancy ended by way of a miscarriage or still-birth in circumstances that give rise to an entitlement to bereavement leave under the Holidays Act 2003.

</details>

<details>
  <summary markdown="span">Family Violence Leave</summary>

* All team members who have been employed for a six month continuous period are entitled to 10 days of paid leave if they have been affected by family violence. 
* This leave may also be taken to support a child who has experienced domestic violence as long as they live with the team member for some of the time. 
* A person is affected by family violence if they are a person against whom any other person is inflicting, or has inflicted family violence, and/or a person with whom there ordinarily or periodically resides a child against whom any other person is inflicting or has inflicted, family violence.
* Team members may also request [short-term flexible working arrangements](https://www.employment.govt.nz/leave-and-holidays/domestic-violence-leave/short-term-flexible-working/) for up to 2 months.  
* This leave runs concurrently with GitLab PTO. Team members should notify Total Rewards (total-rewards@gitlab.com) if they need to make use of this leave.

</details>

### Medical
GitLab does not plan at this time to offer Private Health Insurance benefits because New Zealand residents can access free or subsidised medical care in New Zealand through the public healthcare system. Injuries and accidents are covered by the Accident Compensation Corporation.

### Pension
GitLab's KiwiSaver contributions will be 3% on top of base salary. Team members will automatically be enrolled in the KiwiSaver scheme but may elect to opt out within the first 56 days of employment. To opt-out they should follow the [process on IRD site](https://www.ird.govt.nz/kiwisaver/kiwisaver-individuals/opting-out-of-kiwisaver/opt-out-of-kiwisaver). GitLab will deduct a participating team member's contributions from their before-tax pay at the team member's chosen contribution rate (3%, 4%, 6%, 8% or 10%). If a team member does not choose a contribution rate, the default rate of 3% will be used to calculate deductions. 

### Life Insurance
GitLab does not plan at this time to offer Life Insurance benefits because New Zealanders can access [government payments and services](https://www.workandincome.govt.nz/providers/health-and-disability-practitioners/health-and-disability-related-benefits.html) to help if they get ill, injured or have a disability.
