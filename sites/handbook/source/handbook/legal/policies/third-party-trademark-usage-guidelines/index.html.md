---
layout: handbook-page-toc
title: "Third-party Trademark Usage Guidelines"
description: "Guidelines applicable to the use of third-party trademarks in the GitLab product"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Scope

These guidelines apply to the use of third-party trademarks in the GitLab product. Use of third-party trademarks on GitLab websites, marketing or sales materials, or in other customer or public-facing materials is not covered by these guidelines.

### What is a trademark?

Registered trademarks can be unstylized words or letters (“**wordmarks**”), like _GitLab_, or pictures (“**logomarks**”), like the [GitLab Tanuki](https://about.gitlab.com/images/press/logo/png/gitlab-icon-rgb.png). Sometimes, wordmarks and logomarks are combined to create a logo. These guidelines apply to wordmarks, logomarks and logos, but some parts of it only apply to logomarks and logos. For brevity, throughout these guidelines references to _logo_ shall be taken to mean both logomark and logo.

Trademarks (including logos, and company, product and service names) are the property of the trademark owner. Using a third-party trademark without the owner’s authorisation can constitute trademark infringement and may expose GitLab to liability. Follow these guidelines to minimise the risk of trademark infringement when adding any third-party trademarks to the GitLab product.

### Fair use of third-party trademarks

In certain limited circumstances, use of a trademark is permitted without the authorisation of the owner. This is sometimes referred to as _fair use_ of the trademark.

Use of a third-party trademark must meet **all** of the following criteria to constitute _fair use_:

*   The company, product or service in question cannot be identified without using the trademark.
*   Only so much of the trademark as is necessary to identify the company, product or service is used. This means that using a logo will usually not constitute fair use. For example, you can identify GitLab just by using the wordmark _GitLab_; there is no need to use [GitLab’s logo](https://about.gitlab.com/images/press/logo/png/gitlab-logo-gray-rgb.png). 
*   Use of the trademark does not imply sponsorship or endorsement by the owner.

### Process for adding third-party trademarks to GitLab

Follow these steps when adding new third-party trademarks to GitLab, or using a third-party trademark already in GitLab for a new purpose.

1. **Check the proposed use complies with the Dos and Don’ts.** Legal will not approve requests for proposed use of third-party trademarks that do not comply with the [Dos and Don’ts](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/tree/master/-/sites/handbook/source/handbook/legal/policies/third-party-trademark-usage-guidelines/index.html.md/#dos-donts-for-use-of-third-party-trademarks-in-gitlab) set out in these guidelines.

2. **Consider whether the proposed use constitutes _fair use_**. If you are proposing to add a plain text **wordmark** to GitLab, and the proposed use constitutes _fair use_, legal approval is not required to proceed. The [criteria for fair use](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/tree/master/-/sites/handbook/source/handbook/legal/policies/third-party-trademark-usage-guidelines/index.html.md/#fair-use-of-third-party-trademarks) are set out in these guidelines above. If you are in any doubt whether your proposed use constitutes _fair use_, create a [new legal issue](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=general-legal-template) to discuss with legal before proceeding.
3. **Check the trademark and the proposed use are not already approved**. Legal operates a [Third-party Trademark Tracker](https://docs.google.com/spreadsheets/d/1fa4pzDgbtXSbjw1hex-jouoYu_NDwHpwQwJMbcBHmI4/edit?usp=sharing), which details the trademarks and uses which are approved for use in GitLab. Search this tracker to check if the trademark you are proposing to add, and the proposed use, are already approved. If the trademark you are proposing to add has been approved, but the proposed use has not, a new approval is required.
4. **Add details of the new trademark, or use, to the tracker**. Before contacting legal in Step 5, add as many details you can about the new trademark, or use, to the [Third-party Trademark Tracker](https://docs.google.com/spreadsheets/d/1fa4pzDgbtXSbjw1hex-jouoYu_NDwHpwQwJMbcBHmI4/edit?usp=sharing). 
5. **Create a new third-party trademark approval issue**. Using the [Third-party Trademark Approval issue template](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issuable_template=thirdparty-logo-approval), create a new issue to obtain approval from legal for the new trademark, or the new use.
6. Refer back to these guidelines and this process each time you want to add a new third-party trademark to GitLab, or use an existing trademark for a new purpose. 

### Dos & Don’ts for use of third-party trademarks in GitLab 

When using a third-party trademark in GitLab:

Do not:

*   Use the trademark as part of the name of a GitLab product, service or feature.
*   Use the trademark in a way that implies sponsorship or endorsement by the owner.
*   For logos, alter the trademark in any way. If you need to alter the iconography, colour or interaction of a third-party logo to conform to GitLab’s CSS, reach out to legal to discuss.
*   For logos, combine the trademark with any other symbol, words, image or design.

Do:

*   Use the trademark for referential purposes only, i.e. to refer to another company, product or service, and for no other purpose.
*   Obtain the authorisation of the owner if your proposed use does not constitute _fair use_.
*   For logos, comply with any applicable brand guidelines or other terms of use imposed by the trademark owner. You are responsible for locating and interpreting any applicable brand guidelines; any onerous license terms will be identified and communicated to you by legal as part of the approval process.  
*   For logos, obtain the image file from the owner’s repository of brand assets or another owner-authorized source. The file format can be changed, or the logo recreated in another file format, provided the result is visually identical to the original.
*   For logos, save the image file in the [illustrations/third-party-logos](https://gitlab.com/gitlab-org/gitlab-svgs/-/tree/main/illustrations/third-party-logos) directory in the [gitlab-svgs](https://gitlab.com/gitlab-org/gitlab-svgs) project.
